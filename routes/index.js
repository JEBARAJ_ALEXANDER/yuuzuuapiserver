let router = require('express').Router();

router.use('/partner', require('./partner'));

module.exports = router;