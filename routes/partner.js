let express = require('express');
let PartnerController = require("../controllers/partner");
let AuthenticationController = require("../controllers/authentication");
let router = express.Router();

router.get('/', new PartnerController().index);
router.get('/list', new PartnerController().index);
router.get('/id/:id', new PartnerController().by_id);
router.post('/id/:id', new AuthenticationController().authenticate, new PartnerController().edit);
router.post('/import', new AuthenticationController().authenticate, new PartnerController().importFromCSV);
module.exports = router;
