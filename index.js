let express = require('express');
let ParseServer = require('parse-server').ParseServer;
let path = require('path');
let env = require('node-env-file');

class Server {
  constructor() {
    this.app = express();
    Server.setUpEnv();
    this.parseServer = new ParseServer({
      databaseURI: process.env.MONGODB_URI,
      cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
      appId: process.env.APP_ID,
      masterKey: process.env.MASTER_KEY,
      serverURL: process.env.SERVER_URL,
      liveQuery: {
        classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
      }
    });
    this.setUp();
    Server.setUpDashboard();
  }

  setUp() {
    // Serve static assets from the /public folder
    this.app.use('/public', express.static(path.join(__dirname, '/public')));

    // Serve the Parse API on the /parse URL prefix
    let mountPath = process.env.PARSE_MOUNT;
    this.app.use(mountPath, this.parseServer);

    // Parse Server plays nicely with the rest of your web routes
    this.app.get('/', function (req, res) {
      res.status(200).send('Server started successfully!');
    });

    // There will be a test page available on the /test path of your server url
    // Remove this before launching your app
    this.app.get('/test', function (req, res) {
      res.sendFile(path.join(__dirname, '/public/test.html'));
    });

    this.app.get('/usertest', function (req, res) {
      res.sendFile(path.join(__dirname, '/public/usertest.html'));
    });

    this.app.get('/partneredit', function (req, res) {
      res.sendFile(path.join(__dirname, '/public/partneredittest.html'));
    });

    this.app.use(require('./routes'));
  }

  start() {
    let port = process.env.PORT;
    let httpServer = require('http').createServer(this.app);
    httpServer.listen(port, function () {
      console.log(process.env.SERVER_NAME + ' running on port ' + port + '.');
    });

    // This will enable the Live Query real-time server
    ParseServer.createLiveQueryServer(httpServer);
  }

  static setUpEnv() {
    env(__dirname + '/.env');
  }

  static setUpDashboard() {
    let dashboard = `parse-dashboard --appId ${process.env.APP_ID} --host ${process.env.DASHBOARD_HOST} --port ${process.env.DASHBOARD_PORT} --masterKey ${process.env.MASTER_KEY} --serverURL ${process.env.SERVER_URL} --appName ${process.env.SERVER_NAME}`;
    console.log("Dashboard, run command");
    console.log(dashboard);
  }
}

const server = new Server();
server.start();




