  Parse.initialize('db47f83eab58e00b67485fa10e6966184b820a36');
  Parse.serverURL = 'http://localhost:1337/parse';
function login(username, password) {
  Parse.User.logIn(username, password, {
    success: function (user) {
      console.log(user);
    },
    error: function (user, error) {
      console.log(error);
    }
  });
}

function register(username, password, email) {
  var user = new Parse.User();
  user.set("username", username);
  user.set("password", password);
  user.set("email", email);

  user.signUp(null, {
    success: function (user) {
      console.log("Registration successful");
    },
    error: function (user, error) {
      console.log("Registering failed", "Error: " + error.code + " " + error.message);
      alert();
    }
  });
}

function getSessionToken() {
  var currentUser = Parse.User.current();
  if (currentUser) {
    console.log(currentUser.getSessionToken());
  } else {
    console.log("Errro", "Please login or register!")
  }
}