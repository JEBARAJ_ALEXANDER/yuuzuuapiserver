const Parse = require("parse/node");
Parse.initialize(process.env.APP_ID);
Parse.serverURL = process.env.SERVER_URL;
const ArrayUtils = require("./utils");

module.exports = class Partner extends Parse.Object {
  set(field, value) {
    if (new ArrayUtils(Partner.fieldList()).notContains(field))
      throw 'This field not supported';
    switch (field) {
      case 'shortcut':
        if (String(value).length > 0) {
          throw "Shortcut cannot be blank";
        }
        break;
      case 'status':
        if (isNaN(value)) {
          throw "Status should be a number";
        }
        break;
    }
    super.set(field, value);
  }

  static fieldList() {
    return [
      'name',
      'image_fsref',
      'categories',
      'domains',
      'shortcut',
      'url',
      'affiliate_commission',
      'user_commission',
      'notes',
      'tracking_speed',
      'claim_time',
      'terms',
      'status'
    ]
  }

  constructor() {
    super('Partner');
  }
};