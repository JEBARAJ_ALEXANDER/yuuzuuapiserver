module.exports = class ArrayUtils {
  constructor(array) {
    if (!Array.isArray(array)) {
      throw "not an array";
    }
    this.array = array;
  };

  contains(needle) {
    for (let e of this.array) {
      if (e == needle) return true;
    }
    return false;
  };

  notContains(needle) {
    return !this.contains(needle)
  }
};