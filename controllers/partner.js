const Parse = require("parse/node");
const Partner = require("../models/partner");
let partner = new Partner();

let BaseController = require('./base');

module.exports = class PartnerController extends BaseController {
  constructor() {
    super()
  }

  index(req, res) {
    let query = new Parse.Query(partner);
    query.equalTo("status", "0");
    super.sendPromise(query.find({sessionToken: req.user.sessionToken}), res);
  };

  by_id(req, res) {
    let query = new Parse.Query(partner);
    query.equalTo("objectId", req.params.id);
    super.sendPromise(query.first({sessionToken: req.user.sessionToken}), res);
  };

  edit(req, res) {
    let query = new Parse.Query(partner);
    query.equalTo("objectId", req.params.id);
    let promise = query.first({sessionToken: req.user.sessionToken})
        .then(function (result) {
          if (result === undefined) {
            return Parse.Promise.error("Partner not Found");
          }
          result.set("name", "dummy");
          return result.save(null, {sessionToken: req.user.sessionToken});
        });
    super.sendPromise(promise, res);
  };

  importFromCSV(req, res) {
    let fs = require("fs");
    let csv = require('fast-csv');
    const stream = fs.createReadStream(__dirname + "/../data/yuuzoo_v4_dev_affiliate_partners.csv");
    let partners = [];
    let csvStream = csv()
        .on("data", function (partner) {
          let p = new Partner();
          let fields = Partner.fieldList();
          let i = 4;//Skip first 4 fields
          for (let field of fields) {
            p.set(field, partner[i++]);
          }
          partners.push(p);
        })
        .on("end", function () {
          Parse.Object.saveAll(partners, {sessionToken: req.user.sessionToken})
              .then(result => {
                res.send("Migration Done!")
              }, error => {
                res.send("Migration failed! " + error)
              });
        });
    stream.pipe(csvStream);
  };
};